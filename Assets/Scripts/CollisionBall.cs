﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollisionBall : MonoBehaviour {


    public float forceBall = 0.5f;

    public GameObject Ball;


    public GameObject imgPower;

    public bool collision = false;

		public float distance;

		public bool x, y, z;
		
		public Vector3 axis;
		private Vector3 distancia;
	public float fuerza;
    public Vector3 Vectorfuerza;
    public Vector3 direction;
    public float wallBouncy=10;
	private Vector3 lastBallPosition= new Vector3(0,0,0);
    private float bouncy = 100;

    void Update ()
		{
        if (Input.GetKey(KeyCode.F))
        {
            if (forceBall < 1)
                forceBall += 0.01f;
        }

        if (Input.GetKey(KeyCode.G))
        {
            if (forceBall > 0.1)
                forceBall -= 0.01f;
        }

        imgPower.GetComponent<Image>().fillAmount = forceBall;

        if (Ball == null)
            Ball = GameObject.Find("Ball(Clone)");

        distancia = Ball.transform.position - this.transform.position;
        //Debug.Log(Module(distancia));
			if (Vector3.Dot (distancia, -this.transform.up) < 2 && Module(distancia)<3) {


            collision = true;
			} else {
				collision = false;
			}
			if(!collision)
			{
                direction = new Vector3(0, 0, 0);
			}
			else
			{
			
			fuerza = Module (Ball.transform.position, lastBallPosition) / Time.deltaTime;
            Vectorfuerza = new Vector3(Ball.transform.position.x - lastBallPosition.x, Ball.transform.position.y - lastBallPosition.y, Ball.transform.position.z - lastBallPosition.z)/Time.deltaTime;
            //direction = new Vector3 (this.transform.position.x - Ball.transform.position.x, this.transform.position.y - Ball.transform.position.y, this.transform.position.z - Ball.transform.position.z);

            /*direction = new Vector3(this.transform.position.x - Ball.transform.position.x, this.transform.position.y - Ball.transform.position.y , this.transform.position.z - Ball.transform.position.z );
            direction.Normalize ();*/
            direction = newDirection(Vectorfuerza, this.transform.right, this.transform.forward, this.transform.up);
            direction = new Vector3(direction.x *fuerza, direction.y * fuerza, direction.z * fuerza)*forceBall*bouncy;
			//direction = new Vector3 (0, 30, 250);
			//Debug.Log (direction);	
				
			}
		lastBallPosition = Ball.transform.position;
		}
	private float Module(Vector3 v){
		return Mathf.Sqrt (Mathf.Pow (v.x, 2) + Mathf.Pow (v.y, 2) + Mathf.Pow (v.z, 2));
	}
	private float Module(Vector3 v1,Vector3 v2){
		return Mathf.Sqrt (Mathf.Pow (v1.x-v2.x, 2) + Mathf.Pow (v1.y-v2.y, 2) + Mathf.Pow (v1.z-v2.z, 2));
	}

    /*private Vector3 CrossProduct(Vector3 vectorUp, Vector3 vectorRight)
    {
        Vector3 normal = new Vector3 (vectorUp.y * vectorRight.z - vectorUp.z * vectorRight.y, -1 * (vectorUp.x * vectorRight.z - vectorUp.z * vectorRight.x), vectorUp.y * vectorRight.x - vectorUp.x * vectorRight.y);
        return normal;
    }*/
    private float angleDirection(Vector3 incipient, Vector3 reflectant)
    {
        Vector3 iu = incipient / Module(incipient);
        Vector3 ru = reflectant / Module(reflectant);
        return Vector3.Dot(iu, ru);
    }
    private Vector3 newDirection(Vector3 incipient, Vector3 rx,Vector3 ry, Vector3 rz)
    {
        return new Vector3(angleDirection(incipient, rx), angleDirection(incipient, ry), angleDirection(incipient,rz));
    }
}
 