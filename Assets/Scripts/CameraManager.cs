﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Camera mainCamera;
    public Camera secondaryCamera;

    public RotateMovePala moveScript;

    void Start()
    {
        secondaryCamera.enabled = false;    
    }

    void Update ()
    {
		if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            mainCamera.enabled = true;
            secondaryCamera.enabled = false;

            moveScript.movePala = true;
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            secondaryCamera.enabled = true;
            mainCamera.enabled = false;

            moveScript.movePala = false;
        }
    }
}
