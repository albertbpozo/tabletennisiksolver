﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour {




    public GameObject Ball;

    public bool collision = false;

    public float distance;

    public bool x, y, z;

    public Vector3 axis;
    private Vector3 distancia;
    public float fuerza;
    public Vector3 direction;
    private Vector3 lastBallPosition = new Vector3(0, 0, 0);
    public float bouncy = 10;
    private Vector3 positiveForce;

    void Update()
    {
        if (Ball == null)
            Ball = GameObject.Find("Ball(Clone)");

        distancia = Ball.transform.position - this.transform.position;
        if (Vector3.Dot(distancia, this.transform.up) < 0.5) {
            collision = true;
        } else {
            collision = false;
        }
        if (!collision)
        {
            direction = new Vector3(0, 0, 0);

            //Debug.Log (Vector3.Dot(distancia, this.transform.up));
        }

        else
        {
            //Debug.Log ("collision");
            fuerza = Module(Ball.transform.position, lastBallPosition) / Time.deltaTime;
            positiveForce = Ball.transform.position - lastBallPosition;
            if (positiveForce.x < 0)
            {
                positiveForce.x = -1;
            }
            else
            {
                positiveForce.x = 1;
            }


            if (positiveForce.y < 0)
            {
                positiveForce.y = -1;
            }
            else
            {
                positiveForce.y = 1;
            }

            if (positiveForce.z < 0)
            {
                positiveForce.z = -1;
            }
            else
            {
                positiveForce.z = 1;
            }
            //direction = new Vector3 (this.transform.position.x - Ball.transform.position.x, this.transform.position.y - Ball.transform.position.y, this.transform.position.z - Ball.transform.position.z);
            if (z) {
                direction = new Vector3(cambiDeSigne(positiveForce.x,(this.transform.position.x - Ball.transform.position.x)), cambiDeSigne(positiveForce.y, (this.transform.position.y - Ball.transform.position.y)), this.transform.position.z - Ball.transform.position.z * 10);
                
            }
            if (y)
            {
                direction = new Vector3(cambiDeSigne(positiveForce.x, (this.transform.position.x - Ball.transform.position.x)), this.transform.position.y - Ball.transform.position.y * 50, cambiDeSigne(positiveForce.z, (this.transform.position.z - Ball.transform.position.z)) * 10);

            }
            if (x)
            {
                direction = new Vector3(this.transform.position.x - Ball.transform.position.x * 100, cambiDeSigne(positiveForce.y, (this.transform.position.y - Ball.transform.position.y))* 50, cambiDeSigne(positiveForce.z, (this.transform.position.z - Ball.transform.position.z)));
            }
            direction.Normalize();
            direction *= fuerza * bouncy;
            //direction = new Vector3 (0, 30, 250);
            //Debug.Log (direction);	

        }
        lastBallPosition = Ball.transform.position;
    }
    private float Module(Vector3 v) {
        return Mathf.Sqrt(Mathf.Pow(v.x, 2) + Mathf.Pow(v.y, 2) + Mathf.Pow(v.z, 2));
    }
    private float Module(Vector3 v1, Vector3 v2) {
        return Mathf.Sqrt(Mathf.Pow(v1.x - v2.x, 2) + Mathf.Pow(v1.y - v2.y, 2) + Mathf.Pow(v1.z - v2.z, 2));
    }
    private float cambiDeSigne(float signo, float numero){
        return signo * Mathf.Sqrt(Mathf.Pow(numero, 2));

    }
		/*private Vector3 CrossProduct(Vector3 vectorUp, Vector3 vectorRight)
		{
			Vector3 normal = new Vector3 (vectorUp.y * vectorRight.z - vectorUp.z * vectorRight.y, -1 * (vectorUp.x * vectorRight.z - vectorUp.z * vectorRight.x), vectorUp.y * vectorRight.x - vectorUp.x * vectorRight.y);
			return normal;
		}*/
	}