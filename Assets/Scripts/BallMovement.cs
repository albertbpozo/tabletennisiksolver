﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour {
	public GameObject pla0;

	public GameObject pla1;

	public GameObject pla2;

	public GameObject pla3;

    public GameObject pla4;

    public GameObject pla5;


    public GameObject pla6;

    [SerializeField]
    private Vector3 speed;

    [SerializeField]
    private Vector3 aceleration;

    private Vector3 gravity = new Vector3(0, -9.81f, 0);

    [SerializeField]
    private Vector3 windFriction;

    [SerializeField]
    private Vector3 windSpeed;

    [SerializeField]
    public Vector3 forceAplyied;

    private float  WIND_COEFICIENT = 0.02f;

    [SerializeField]
    private float mass;

    [SerializeField]
    public float radius;
	private bool salida;
    
    private void Start()
    {
        this.pla0 = GameObject.Find("Plane");
        this.pla1 = GameObject.Find("Plane1");
        this.pla2 = GameObject.Find("Plane2");
        this.pla3 = GameObject.Find("Plane3");
        this.pla4 = GameObject.Find("Plane4");
        this.pla5 = GameObject.Find("Plane5");
        this.pla6 = GameObject.Find("PlanePala");
        this.transform.position = new Vector3(0, 12, 8);
        this.speed = new Vector3(0, 15, -80);
        this.aceleration = new Vector3(0, 0, 0);
        this.forceAplyied = new Vector3(0, 0, 0);
        this.windFriction = new Vector3(0, 0, 0);
        this.windSpeed = new Vector3(0, 0, 0);
        this.gravity = new Vector3(0, -9.81f, 0);
        salida = false;
        gravity *= mass;
    }

    void Update ()
    {
        if (Input.GetKeyDown("space"))
            Restart();
        if (Vector3.Magnitude(speed) != 0)
        {
            

            windFriction = (-speed * WIND_COEFICIENT) * Time.deltaTime;
            if (pla0.GetComponent<Collision>().direction.magnitude != 0 || pla1.GetComponent<Collision>().direction.magnitude != 0 || pla2.GetComponent<Collision>().direction.magnitude != 0 || pla3.GetComponent<Collision>().direction.magnitude != 0 || pla4.GetComponent<Collision>().direction.magnitude != 0 || this.transform.position.z < pla3.transform.position.z || this.transform.position.y < pla0.transform.position.y || pla5.GetComponent<Collision>().direction.magnitude != 0 || pla6.GetComponent<CollisionBall>().direction.magnitude != 0)
            {
                if (!salida)
                {
                    speed = new Vector3(0, 0, 0);
                    aceleration = mass * (pla0.GetComponent<Collision>().direction + pla1.GetComponent<Collision>().direction + pla1.GetComponent<Collision>().direction + pla5.GetComponent<Collision>().direction + pla2.GetComponent<Collision>().direction + pla3.GetComponent<Collision>().direction + pla4.GetComponent<Collision>().direction + pla6.GetComponent<CollisionBall>().direction) * Time.deltaTime;
                    salida = true;
                }
            }
            else
            {
                salida = false;
                aceleration = (gravity + windFriction + windSpeed + forceAplyied + pla0.GetComponent<Collision>().direction + pla1.GetComponent<Collision>().direction + pla2.GetComponent<Collision>().direction + pla3.GetComponent<Collision>().direction + pla4.GetComponent<Collision>().direction + pla6.GetComponent<CollisionBall>().direction) * Time.deltaTime;
            }
            speed += aceleration;
            if (Vector3.Magnitude(speed) < 0.3)
            {
                speed *= 0;
            }

            this.transform.position += (speed * Time.deltaTime);
        }
        RendererForces.DrawVector(new Vector3(0, -9.81f, 0) * 0.4f, RendererForces.Vectors.RED, this.transform);
        RendererForces.DrawVector(speed, RendererForces.Vectors.GREEN, this.transform);
    }

    public void Restart()
    {
        Destroy(this.gameObject);
        Instantiate(Resources.Load("Ball") as GameObject, new Vector3(0, 0, 0), Quaternion.identity);
    }
}
