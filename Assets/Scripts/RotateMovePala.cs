﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMovePala : MonoBehaviour {

    public bool movePala = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (movePala)
        {
            if (Input.GetMouseButton(0))
            {
                this.transform.Rotate(new Vector3(0, 0, -30) * Time.deltaTime * 10);
            }

            else if (Input.GetMouseButton(1))
            {
                this.transform.Rotate(new Vector3(0, 0, 30) * Time.deltaTime * 10);
            }

            this.transform.Rotate(new Vector3(Input.GetAxis("Mouse ScrollWheel"), 0, 0) * Time.deltaTime * 3000);

            Vector3 screenPoint = Input.mousePosition;
            screenPoint.z = 13.0f;
            //screenPoint.x = -screenPoint.x;
            transform.position = Camera.main.ScreenToWorldPoint(screenPoint) + new Vector3((screenPoint.x / 300), screenPoint.y / 88, 0);
        }
    }
}
